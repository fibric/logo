# 'fibric' as a logo

## PNG
<img src="https://gitlab.com/fibric/logo/-/design_management/designs/86230/4e09e09bd46578a333889929bdc6ce23c556bea0/raw_image" align="center" width="128" alt="'fibric' as a logo">

## SVG

<img src="https://gitlab.com/fibric/logo/-/raw/master/fibric-logo-text.svg" align="center" width="128" alt="'fibric' as a logo">
